﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {

    public Text text;

    private enum States {cell, mirror, sheets_0, lock_0, cell_mirror, sheets_1, lock_1, corridor_0, stairs_0, stairs_1, stairs_2, courtyard, floor, corridor_1, corridor_2, corridor_3, closet_door, in_closet};
    private States myState;

    // Use this for initialization
    void Start ()
    {
        myState = States.cell;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (myState == States.cell)
        {
            cell();
        }
        else if (myState == States.sheets_0)
        {
            sheets_0();
        }
        else if (myState == States.sheets_1)
        {
            sheets_1();
        }
        else if (myState == States.lock_0)
        {
            lock_0();
        }
        else if (myState == States.lock_1)
        {
            lock_1();
        }
        else if (myState == States.mirror)
        {
            mirror();
        }
        else if (myState == States.cell_mirror)
        {
            cell_mirror();
        }
        else if (myState == States.corridor_0)
        {
            corridor_0();
        }
        else if (myState == States.stairs_0)
        {
            stairs_0();
        }
        else if (myState == States.stairs_1)
        {
            stairs_1();
        }
        else if (myState == States.stairs_2)
        {
            stairs_2();
        }
        else if (myState == States.courtyard)
        {
            courtyard();
        }
        else if (myState == States.floor)
        {
            floor();
        }
        else if (myState == States.corridor_1)
        {
            corridor_1();
        }
        else if (myState == States.corridor_2)
        {
            corridor_2();
        }
        else if (myState == States.corridor_3)
        {
            corridor_3();
        }
        else if (myState == States.closet_door)
        {
            closet_door();
        }
        else if (myState == States.in_closet)
        {
            in_closet();
        }
    }

    #region State handler methods
    void cell ()
    {
        text.text = "Oh no, I am stuck in my cell, whatever shall I do? I should never have murdered those babies. I belong here... But I would also like to escape... The door is cunningly locked from the outside. " +
                    "There is a mirror on the wall of the almost empty cell, and I have a bed with some bedsheets. Maybe I could use one of these items to escape somehow?\n\n" +
                    "Press S to view Sheets, M to view Mirror and L to view Lock";

        if (Input.GetKeyDown(KeyCode.S))
        {
            myState = States.sheets_0;
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            myState = States.mirror;
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            myState = States.lock_0;
        }
    }

    void sheets_0()
    {
        text.text = "Damn these stink. I wish that the last prisoner wasn't such a fat mess... Freeb I think his name was...\n\n" +
                    "Press R to return to your cell";

        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.cell;
        }
    }

    void mirror()
    {
        text.text = "The dirty old mirror on the wall seems loose.\n\n" +
                    "Press T to Take the mirror or R to Return to your cell";

        if (Input.GetKeyDown(KeyCode.T))
        {
            myState = States.cell_mirror;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.cell;
        }
    }

    void cell_mirror()
    {
        text.text = "You are still in your cell, but you have a nice shiny(ish) mirror in your hand. Maybe this could be of some sort of use for something?\n\n" +
                    "Press S to view Sheets or L to view Lock";

        if (Input.GetKeyDown(KeyCode.S))
        {
            myState = States.sheets_1;
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            myState = States.lock_1;
        }
    }

    void sheets_1()
    {
        text.text = "Holding a mirror in your hand doesn't make the sheets stink any less of piss.\n\n" +
                    "Press R to return to your cell";

        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.cell_mirror;
        }
    }

    void lock_0()
    {
        text.text = "This is one of those combination button locks. You have no idea what the combination is. You remember that the last prisoner also escaped from this cell..." +
                    "He ate alot of KFC... Maybe, his greasy fingerprints are still on the buttons... If only there was some way to see...\n\n" +
                    "Press R to Return to your cell";

        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.cell;
        }
    }

    void lock_1()
    {
        text.text = "You carefully put the mirror through the bars, and turn it round so you can see the lock. You can just about make out some greasy fingerprints on four of the buttons. " +
                    "You press the greasy butttons and hear a click.\n\n" +
                    "Press O to Open, or R to Return to your cell";

        if (Input.GetKeyDown(KeyCode.O))
        {
            myState = States.corridor_0;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.cell_mirror;
        }
    }

    void corridor_0()
    {
        text.text = "You have escaped from your cell, but the prison is full of guards. You know the stairs leads to a courtyard that is attached to the exit, but this will almost certainly lead to being caught or worse.\n\n" +
                    "Next to the stairs, there is a closet. There is also a pile of fag ends and chicken grease on the floor next to the cell door.\n\n" +
                    "Press C to inspect the Closet, F to inpect the greasy Fag ends, or S to climb the Stairs";

        if (Input.GetKeyDown(KeyCode.C))
        {
            myState = States.closet_door;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            myState = States.floor;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            myState = States.stairs_0;
        }
    }

    void stairs_0()
    {
        text.text = "You start to walk up the stairs towards the outside light. You hear the noise of guards chatting in the courtyard. With no way to get past unnoticed, you sneak back down the stairs and reconsider.\n\n" +
                    "Press R to Return to the corridor";

        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.corridor_0;
        }
    }

    void stairs_1()
    {
        text.text = "Tempting as it is to murder the entire group of armed guards with the paper clip, you realise it would probably be best to remain unnoticed.\n\n" +
                    "Press R to Return to the corridor";

        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.corridor_1;
        }
    }

    void stairs_2()
    {
        text.text = "You climb the stairs to your inevitable death. Weilding the now bent paperclip like a maniac you are gunned down in seconds.\n\n" +
                    "Press P to Play again";

        if (Input.GetKeyDown(KeyCode.P))
        {
            myState = States.cell;
        }
    }

    void courtyard()
    {
        text.text = "You walk through the courtyard dressed as a cleaner. Amazingly nobody recognises you. A guard tips his hat to you as you walk past, claiming your freedom.\n\n" +
                    "Press P to play again";

        if (Input.GetKeyDown(KeyCode.P))
        {
            myState = States.cell;
        }
    }

    void floor()
    {
        text.text = "You rummage through the pile of chicken grease and fag ends on the floor next to the cell door. Rummaging through it, you find a greasy old rusted paperclip.\n\n" +
                    "Press L to Leave the paperclip on the floor, or P to take the Paperclip";

        if (Input.GetKeyDown(KeyCode.L))
        {
            myState = States.corridor_0;
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            myState = States.corridor_1;
        }
    }

    void corridor_1()
    {
        text.text = "Still in the corridor, but with paperclip in slightly greasier, nicotine stained hand. You wonder if the closet would succumb to some lock picking.\n\n" +
                    "Press P to Pick the lock, or S to climb the Stairs";

        if (Input.GetKeyDown(KeyCode.P))
        {
            myState = States.in_closet;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            myState = States.stairs_1;
        }
    }

    void corridor_2()
    {
        text.text = "You return to the corridor, considering an honorable death the only option. If only they had some cool uniform, like an army officer or fireman.\n\n" +
                    "Press C to revisit the Closet, or S to climb the Stairs";

        if (Input.GetKeyDown(KeyCode.C))
        {
            myState = States.in_closet;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            myState = States.stairs_2;
        }
    }

    void corridor_3()
    {
        text.text = "You are now standing back in the corridor, but convincingly dressed as a cleaner. The stench of chicken grease and nicotine still wafts over you as you consider the run for freedom.\n\n" +
                    "Press S to climb the Stairs, or U to undress";

        if (Input.GetKeyDown(KeyCode.S))
        {
            myState = States.courtyard;
        }
        else if (Input.GetKeyDown(KeyCode.U))
        {
            myState = States.in_closet;
        }
    }

    void closet_door()
    {
        text.text = "You examine the closet door. The handle is greasy, but it's locked. Maybe there is something nearby that you can use to encourage it to open.\n\n" +
                    "Press R to Return to the corridor";

        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.corridor_0;
        }
    }

    void in_closet()
    {
        text.text = "You are inside the closet. It stinks of nicotine and grease in here, phew. You rummage through the inmates uniforms and find an odd cleaners uniform. It looks about your size.\n\n" +
                    "Press D to Dress up in the uniform, or R to Return to the corridor";


        if (Input.GetKeyDown(KeyCode.D))
        {
            myState = States.corridor_3;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            myState = States.corridor_2;
        }
    }
    #endregion
}
